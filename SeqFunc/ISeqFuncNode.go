package SeqFunc

import (
	"git.oschina.net/yangdao/SGLisp"
	"git.oschina.net/yangdao/SGLisp/parse"
)

type ISeqFuncNode interface {
	OnCall(*SGLisp.Context)
	Update(float64)
	Info() *SeqFuncNodeInfo
}

//阻塞类的序列函数
type IBlockSeqFuncNode interface {
	OnLoad([]parse.Node)
	OnExit()
	OnCall(*SGLisp.Context)
	Update(float64)
	Info() *SeqFuncNodeInfo
}

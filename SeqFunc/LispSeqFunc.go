package SeqFunc

import (
	"git.oschina.net/yangdao/SGLisp"
	. "git.oschina.net/yangdao/SGLisp/parse"
	//. "git.oschina.net/yangdao/extlib/data_type"
	"fmt"
)

var PkgContext *SGLisp.Context

func init() {
	PkgContext = SGLisp.NewContext(nil)
	PkgContext.DefineFunc("def-func-seq", def_func_seq)
}

/*
 (def-func-seq []
    (log "nmb")
	(wait 0.5)
    (create-unit 447)
 )

*/
func def_func_seq(args []Node, context *SGLisp.Context) Node {
	retFuncSeq := NewSeqFunc(context)
	//填入参数
	func_seq_args := args[0].(*VectorNode)
	for i := 0; i < len(func_seq_args.Nodes); i++ {
		symNode := func_seq_args.Nodes[i].(*SymbolNode)
		retFuncSeq.FuncContext.Define(symNode.Val, SGLisp.StaticNilNode)
	}
	for i := 1; i < len(args); i++ {
		lstNode := args[i].(*ListNode)
		SymName := lstNode.Children()[0].(*SymbolNode).Val
		FuncNode := context.LookUp(SymName)
		if FuncNode != nil {
			switch FuncNode.(type) {
			case SGLisp.NativeFunction:
				seqFuncNode := &NonBlockSeqFuncNode{}
				seqFuncNode.SeqFuncType = SeqFuncNodeType_NonBlock
				seqFuncNode.LispNode = lstNode
				seqFuncNode.Name = SymName
				retFuncSeq.AddSeqFunc(seqFuncNode)
			case *ObjectNode:
				blockNode := FuncNode.(*ObjectNode).Val.(func() IBlockSeqFuncNode)
				newSeqFuncNode := blockNode().(IBlockSeqFuncNode)
				newSeqFuncNode.Info().Name = SymName
				newSeqFuncNode.Info().LispNode = lstNode
				newSeqFuncNode.OnLoad(lstNode.Children())
				retFuncSeq.AddSeqFunc(newSeqFuncNode)
			}

		} else {
			fmt.Println("not Find", SymName)
		}
	}
	return &ObjectNode{Val: retFuncSeq}
}

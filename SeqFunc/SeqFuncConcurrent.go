package SeqFunc

import (
	//"fmt"
	. "git.oschina.net/yangdao/extlib/data_type"
)

//SeqFunc并行容器
type SeqFuncConcurrent struct {
	RawSeqFunc  *SeqFunc
	seqFuncPool *TypVector
}

func NewSeqFuncConcurrent(seqFunc *SeqFunc) *SeqFuncConcurrent {
	return &SeqFuncConcurrent{RawSeqFunc: seqFunc, seqFuncPool: Vector()}
}

func (this *SeqFuncConcurrent) GetSeqFuncInst() *SeqFunc {
	for i := 0; i < this.seqFuncPool.Count(); i++ {
		seqFunc := this.seqFuncPool.Nth(i).(*SeqFunc)
		if seqFunc.IsRun == false {
			return seqFunc
		}
	}
	cloneSeqFunc := this.RawSeqFunc.Clone()
	this.seqFuncPool.Conj(cloneSeqFunc)
	return cloneSeqFunc
}

func (this *SeqFuncConcurrent) Run() {
	this.GetSeqFuncInst().Run()
}

func (this *SeqFuncConcurrent) Update(dt float64) {
	for i := 0; i < this.seqFuncPool.Count(); i++ {
		seqFunc := this.seqFuncPool.Nth(i).(*SeqFunc)
		if seqFunc.IsRun {
			seqFunc.Update(dt)
		}
	}
}

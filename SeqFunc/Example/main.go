package main

import (
	"time"

	"git.oschina.net/yangdao/SGLisp"
	"git.oschina.net/yangdao/SGLisp/SeqFunc"
	"git.oschina.net/yangdao/SGLisp/SeqFunc/builtin"
	"git.oschina.net/yangdao/SGLisp/parse"
)

var seqFuncConcurrent *SeqFunc.SeqFuncConcurrent

func main() {
	SGLisp.InitRuntime()
	CurCtx := SGLisp.NewContext(SeqFunc.PkgContext)
	SeqFunc.PkgContext.SetParent(SGLisp.MainContext)
	CurCtx.Define("wait", &parse.ObjectNode{Val: builtin.NewWaitSeqNode})

	lispStr := `
	(def-func-seq []
	   (log "start")
	   (wait 2.0)
       (log "nmb")
	)`
	seqFunc := SGLisp.EvalValueFormString(lispStr, CurCtx).(*SeqFunc.SeqFunc)
	seqFuncConcurrent = SeqFunc.NewSeqFuncConcurrent(seqFunc)

	Run()
}

func Run() {
	seqFuncConcurrent.Run()
	seqFuncConcurrent.Run()
	tick := time.NewTicker(time.Millisecond * 500)
	for _ = range tick.C {
		seqFuncConcurrent.Update(1)
	}
}

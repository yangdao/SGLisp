package SeqFunc

import "git.oschina.net/yangdao/SGLisp"
import "git.oschina.net/yangdao/SGLisp/parse"

const (
	SeqFuncNodeType_Block    = 0
	SeqFuncNodeType_NonBlock = 1
)

type SeqFuncNodeInfo struct {
	Name        string
	SeqFuncType int
	LispNode    *parse.ListNode
	IsFinish    bool
}

type NonBlockSeqFuncNode struct {
	SeqFuncNodeInfo
}

func (this *NonBlockSeqFuncNode) Info() *SeqFuncNodeInfo {
	return &this.SeqFuncNodeInfo
}

func (this *NonBlockSeqFuncNode) OnCall(context *SGLisp.Context) {
	SGLisp.Evaluate(this.LispNode, context)
}

func (this *NonBlockSeqFuncNode) Update(dt float64) {

}

package builtin

import (
	//"fmt"
	"git.oschina.net/yangdao/SGLisp"
	"git.oschina.net/yangdao/SGLisp/SeqFunc"
	"git.oschina.net/yangdao/SGLisp/parse"
	"time"
)

type WaitSeqNode struct {
	SeqFunc.SeqFuncNodeInfo
	WaitTime int64
	EndTime  int64
}

func NewWaitSeqNode() SeqFunc.IBlockSeqFuncNode {
	return &WaitSeqNode{}
}

func (this *WaitSeqNode) OnLoad(args []parse.Node) {
	args = SGLisp.EvalArgs(args, SGLisp.MainContext)
	timef := args[1].(*parse.NumberNode).Val.(float64)
	this.WaitTime = int64(float64(1000000000) * timef)
}

func (this *WaitSeqNode) OnCall(*SGLisp.Context) {
	this.EndTime = time.Now().UnixNano() + this.WaitTime
	//fmt.Println(this.EndTime)
}
func (this *WaitSeqNode) Info() *SeqFunc.SeqFuncNodeInfo {
	return &this.SeqFuncNodeInfo
}

func (this *WaitSeqNode) OnExit() {

}

func (this *WaitSeqNode) Update(dt float64) {
	//fmt.Println(this.EndTime, ">=", time.Now().UnixNano())
	if this.EndTime <= time.Now().UnixNano() {
		this.IsFinish = true
	}
}
